# sched

a little scheduling utility written in go

## installation
```bash
go get git.envs.net/lel/sched
```

## usage

Add an event:
```bash
$ sched -add '1/2/20 @ 4:15pm' 'closing shift at work'
```

Can also use full year in date format if you're scheduling something for a hundred fucking years from now, I guess.

List events:
```bash
$ sched -list
[0] 4:15pm, 1/2/20 ~ closing shift at work
```

List upcoming events:
```bash
$ sched -upcoming
[0] 4:15pm, 1/2/20 ~ closing shift at work
```

List past events:
```bash
$ sched -past
[0] 4:15pm, 1/2/19 ~ closing shift at work
```

Get next event after current time:
```bash
$ sched
4:15pm, 1/2/20 ~ closing shift at work
```

Remove an event:
```bash
$ sched -remove [index number, from -list, -upcoming, or -past]
```

## misc. notes

the code is messier than shit -- sorry -- it seems to work totally fine.