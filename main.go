package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strconv"
	"time"
)

func usage() {
	callName := os.Args[0]
	fmt.Fprintf(os.Stderr, "usage: %s [options]\n", callName)
	fmt.Fprintf(os.Stderr, "\tSchedule some events.\n\n")
	fmt.Fprintf(os.Stderr, "options:\n")
	fmt.Fprintf(os.Stderr, "  -list\n\t\tlist all events\n")
	fmt.Fprintf(os.Stderr, "  -past\n\t\tlist all past events\n")
	fmt.Fprintf(os.Stderr, "  -upcoming\n\t\tlist all upcoming events\n")
	fmt.Fprintf(os.Stderr, "  -add time description\n\t\tadd a new event; fmt time as \"1/2/06 @ 3:04pm\" or \"1/2/2006 @ 3:04pm\"\n")
	fmt.Fprintf(os.Stderr, "  -remove index\n\t\tremove an event by index (get index from -list/-past/-upcoming)")
	fmt.Fprintf(os.Stderr, "\n  With no flags, gets next upcoming event.\n")
	os.Exit(2)
}

type Event struct {
	Time time.Time
	Desc string
}

func (e Event) String() string {
	dateFmted := e.Time.Format("1/2")
	timeFmted := e.Time.Format("3:04pm")
	timeDateFmted := fmt.Sprintf("%s, %s", timeFmted, dateFmted)
	oneWeek, _ := time.ParseDuration("168h")
	weekday := e.Time.Weekday().String()[0:2]
	if time.Until(e.Time) < oneWeek && time.Now().Before(e.Time) {
		timeDateFmted = fmt.Sprintf("%s, %v", timeFmted, weekday)
	}
	nowYear := strconv.Itoa(time.Now().Year())
	eventYear := strconv.Itoa(e.Time.Year())
	fmtYear := ""
	if nowYear[:len(nowYear)-2] == eventYear[:len(eventYear)-2] {
		if nowYear == eventYear {
			fmtYear = ""
		} else {
			fmtYear = "/" + eventYear[len(eventYear)-2:]
		}
	} else {
		fmtYear = "/" + eventYear
	}
	timeDateFmted = fmt.Sprintf("%s%s", timeDateFmted, fmtYear)
	return fmt.Sprintf("%s ~ %s", timeDateFmted, e.Desc)
}

func parseFile(file *os.File) ([]Event, error) {
	byteFile, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}
	var events []Event
	err = json.Unmarshal(byteFile, &events)
	if err != nil {
		return nil, err
	}
	sort.SliceStable(events, func(i, j int) bool {
		return events[i].Time.Before(events[j].Time)
	})
	return events, nil
}

func next(file *os.File, currentTime time.Time) (Event, error) {
	scanner := bufio.NewScanner(file)
	now := time.Now()
	var nextEvent Event
	events, err := parseFile(file)

	if err != nil {
		return *new(Event), err
	}

	if len(events) < 1 {
		return *new(Event), errors.New("empty sched-file")
	}

	for _, currEvent := range events {
		if currEvent.Time.After(now) {
			nextEvent = currEvent
			break
		}
	}

	if err := scanner.Err(); err != nil {
		return *new(Event), err
	}
	if nextEvent == *new(Event) {
		return *new(Event), errors.New("no future events")
	}
	return nextEvent, nil
}

func getFilename(filename string) (string, error) {
	if filename == "" {
		filename = path.Join(os.Getenv("HOME"), ".local/share/sched/sched.json")
	}
	folder := filepath.Dir(filename)
	if _, err := os.Stat(folder); err != nil {
		err := os.MkdirAll(folder, os.ModePerm)
		if err != nil {
			return "", err
		}
	}
	return filename, nil
}

func main() {
	flagAdd := flag.Bool("add", false, "add an event")
	flagUpcoming := flag.Bool("upcoming", false, "list upcoming events")
	flagPast := flag.Bool("past", false, "list past events")
	flagList := flag.Bool("list", false, "list all events")
	flagFile := flag.String("file", "", "change file for sched")
	flagRemove := flag.Int("remove", -1, "delete an event by index")

	log.SetPrefix("sched: ")
	log.SetFlags(0)
	flag.Usage = usage
	flag.Parse()

	filename, err := getFilename(*flagFile)
	if err != nil {
		log.Fatal(err)
	}
	file, err := os.OpenFile(filename,
		os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	if *flagAdd {
		if flag.NArg() != 2 {
			usage()
		}
		newTime, err := time.ParseInLocation("1/2/06 @ 3:04pm", flag.Arg(0), time.Local)
		if err != nil {
			newTime, err = time.ParseInLocation("1/2/2006 @ 3:04pm", flag.Arg(0), time.Local)
			if err != nil {
				log.Fatal(err)
			}
		}
		e := Event{newTime, flag.Arg(1)}
		err = add(file, e)
		if err != nil {
			log.Fatal(err)
		}
		return
	}

	// beyond this point we don't want any args
	if flag.NArg() != 0 {
		usage()
	}

	if *flagList {
		err := listAll(file)
		if err != nil {
			log.Fatal(err)
		}
		return
	}

	if *flagUpcoming {
		err := listUpcoming(file)
		if err != nil {
			log.Fatal(err)
		}
		return
	}

	if *flagPast {
		err := listPast(file)
		if err != nil {
			log.Fatal(err)
		}
		return
	}

	if *flagRemove != -1 {
		err := removeEvent(file, *flagRemove)
		if err != nil {
			log.Fatal(err)
		}
		return
	}

	nextEvent, err := next(file, time.Now())
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(nextEvent.String())
}

func add(file *os.File, e Event) error {
	events, err := parseFile(file)
	events = append(events, e)
	err = save(file, events)
	return err
}

func save(file *os.File, events []Event) error {
	byteFile, err := json.MarshalIndent(events, "", "\t")
	if err != nil {
		return err
	}
	file.Truncate(0) // comment or uncomment
	file.Seek(0, 0)  // these lines to see the difference
	_, err = file.Write(byteFile)

	return err
}

func getUpcoming(events []Event) ([]Event, []int) {
	var upcoming []Event
	var indices []int
	for i, nextEvent := range events {
		if nextEvent.Time.After(time.Now()) {
			upcoming = append(upcoming, nextEvent)
			indices = append(indices, i)
		}
	}

	return upcoming, indices
}

func getPast(events []Event) ([]Event, []int) {
	var past []Event
	var indices []int
	for i, nextEvent := range events {
		if nextEvent.Time.Before(time.Now()) {
			past = append(past, nextEvent)
			indices = append(indices, i)
		}
	}

	return past, indices
}

func removeEvent(file *os.File, i int) error {
	events, err := parseFile(file)
	if err != nil {
		return err
	}
	if len(events) < 1 {
		return errors.New("empty sched")
	}
	if len(events) < i || i < 0 {
		return errors.New("invalid index")
	}
	if len(events) == i+1 {
		events = events[:i]
	} else {
		events = append(events[:i], events[i+1:len(events)]...)
	}
	err = save(file, events)
	return err
}

func listAll(file *os.File) error {
	events, err := parseFile(file)
	if err != nil {
		return err
	}

	if len(events) < 1 {
		return errors.New("empty sched-file")
	}
	hasAfter := false
	for i, event := range events {
		if event.Time.After(time.Now()) && hasAfter == false {
			fmt.Printf("\n[*] %s\n\n", Event{time.Now(), "RIGHT NOW!"}.String())
			hasAfter = true
		}
		fmt.Printf("[%v] %s\n", i, event.String())
	}

	return nil
}

func listPast(file *os.File) error {
	events, err := parseFile(file)
	if err != nil {
		return err
	}

	if len(events) < 1 {
		return errors.New("empty sched-file")
	}

	past, indices := getPast(events)

	if len(past) < 1 {
		return errors.New("no past events")
	}

	for i := range past {
		fmt.Printf("[%v] %s\n", indices[i], past[i].String())
	}

	return nil
}

func listUpcoming(file *os.File) error {
	events, err := parseFile(file)
	if err != nil {
		return err
	}

	if len(events) < 1 {
		return errors.New("empty sched-file")
	}

	upcoming, indices := getUpcoming(events)

	if len(upcoming) < 1 {
		return errors.New("no upcoming events")
	}

	for i := range upcoming {
		fmt.Printf("[%v] %s\n", indices[i], upcoming[i].String())
	}

	return nil
}
